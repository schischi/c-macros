#ifndef HASHTBL_H
# define HASHTBL_H

# include "list.h"

# define HASHTBL(type, keyType, name)                                  \
    struct item_##name##_s {                                           \
        keyType key;                                                   \
        type value;                                                    \
    };                                                                 \
    typedef struct item_##name##_s *item_##name##_t;                   \
    LIST(struct item_##name##_s, items_##name);                        \
    struct name##_s {                                                  \
        items_##name##_t *bucket;                                      \
        size_t size;                                                   \
        int(*hash)(keyType);                                           \
        int(*cmp)(keyType, keyType);                                   \
        item_##name##_t it;                                            \
    };                                                                 \
    typedef struct name##_s *name##_t;

# define HASHTBL_INIT(table, s, hash, cmp, f)                          \
    do {                                                               \
        size_t i;                                                      \
        table = malloc(sizeof(*table));                                \
        table->hash = hash;                                            \
        table->cmp = cmp;                                              \
        table->size = s;                                               \
        table->bucket = malloc(sizeof(*table->bucket) * s);            \
        for(i = 0; i < s; ++i)                                         \
            LIST_INIT(table->bucket[i], f);                            \
    }while(0)

# define HASHTBL_SET(table, v, k)                                      \
    do {                                                               \
        int index = table->hash(k);                                    \
        table->it = malloc(sizeof(*table->it));                        \
        table->it->key = k;                                            \
        table->it->value = v;                                          \
        LIST_INSERT_HEAD(table->bucket[index], *table->it);            \
        free(table->it);                                               \
    }while(0)

# define HASHTBL_GET(table, k, ret)                                    \
    do {                                                               \
        int index = table->hash(k);                                    \
        LIST_FOREACH(table->bucket[index]) {                           \
            if(!table->cmp(table->bucket[index]->it->data.key, k)) {   \
                ret = table->bucket[index]->it->data.value;            \
                break;                                                 \
            }                                                          \
        }                                                              \
    }while(0)

# define HASHTBL_DEL(table, k)                                         \
    do {                                                               \
        int index = table->hash(k);                                    \
        LIST_FOREACH(table->bucket[index]) {                           \
            if(!table->cmp(table->bucket[index]->it->data.key, k)) {   \
                LIST_REMOVE(table->bucket[index],                      \
                        table->bucket[index]->it);                     \
                break;                                                 \
            }                                                          \
        }                                                              \
    }while(0)

# define HASHTBL_FREE(table)                                           \
    do {                                                               \
        size_t i;                                                      \
        for(i = 0; i < table->size; ++i)                               \
            LIST_FREE(table->bucket[i]);                               \
        free(table->bucket);                                           \
        free(table);                                                   \
    }while(0)

# define HASHTBL_DEBUG(table)                                          \
    do {                                                               \
        size_t i;                                                      \
        for(i = 0; i < table->size; ++i) {                             \
            fprintf(stderr, "\n%ld : ", i);                            \
            LIST_FOREACH(table->bucket[i])                             \
                fprintf(stderr, "%d -> ",                              \
                        table->bucket[i]->it->data.value);             \
        }                                                              \
        fprintf(stderr, "\n");                                         \
    }while(0)

#endif
