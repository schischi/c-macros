#ifndef VECTOR_H
# define VECTOR_H

# define VECTOR(type, name)                                            \
    struct name##_s {                                                  \
        size_t size;                                                   \
        size_t maxSize;                                                \
        void   (*free)(type);                                          \
        type   *array;                                                 \
    };                                                                 \
    typedef struct name##_s *name##_t;

# define VECTOR_CREATE(vect, f)                                        \
    do {                                                               \
        vect = malloc(sizeof(*vect));                                  \
        vect->size = 0;                                                \
        vect->maxSize = 8;                                             \
        vect->free = f;                                                \
        vect->array = malloc(sizeof(*vect->array) * 8);                \
    }while(0)

# define VECTOR_EXTEND(vect)                                           \
    do {                                                               \
        vect->maxSize *= 2;                                            \
        vect->array = realloc(vect->array, sizeof(*vect->array) *      \
            vect->maxSize);                                            \
    }while(0)

# define VECTOR_REDUCE(vect)                                           \
    do {                                                               \
        vect->maxSize /= 2;                                            \
        vect->array = realloc(vect->array, sizeof(*vect->array) *      \
            vect->maxSize);                                            \
    }while(0)

# define VECTOR_ADD(vect, value)                                       \
    do {                                                               \
        if(vect->size == vect->maxSize)                                \
            VECTOR_EXTEND(vect);                                       \
        vect->array[vect->size++] = value;                             \
    }while(0)

# define VECTOR_DEL(vect, n)                                           \
    do {                                                               \
        vect->array[n] = vect->array[vect->size-- - 1];                \
        if(vect->size < vect->maxSize / 2)                             \
            VECTOR_REDUCE(vect);                                       \
    }while(0)

# define VECTOR_DEL_ORDER(vect, n)                                     \
    do {                                                               \
        size_t i;                                                      \
        for(i = n; i < vect->size - 1; ++i)                            \
            vect->array[i] = vect->array[i + 1];                       \
        --vect->size;                                                  \
    }while(0)

# define VECTOR_FREE(vect)                                             \
    do {                                                               \
        size_t i;                                                      \
        if(vect->free)                                                 \
            for(i = 0; i < vect->size; ++i)                            \
                vect->free(vect->array[i]);                            \
        free(vect->array);                                             \
        free(vect);                                                    \
    }while(0)

# define VECTOR_FOREACH(vect, it)                                      \
    for(it = 0; it < vect->size; ++it)

#endif
