#ifndef LOG_H
# define LOG_H

# include <stdio.h>     /* fprintf  */
# include <stdlib.h>    /* exit     */
# include <errno.h>     /* errno    */
# include <string.h>    /* strerror */

# define LOG_ENABLE 1
# define WARN_ENABLE 2
# define ERROR_ENABLE 4
# define FATAL_ENABLE 8

/*
 *   # define LOGV (LOG_ENABLE | WARN_ENABLE | ERROR_ENABLE | FATAL_ENABLE)
 */

# define log_info(fmt, ...)                                       \
    do {                                                          \
        if(LOGV & LOG_ENABLE) {                                   \
            fprintf(stdout, "%-8s "fmt" (%s:%d)\n", "[INFO]",     \
                __VA_ARGS__, __FILE__, __LINE__);                 \
            fflush(stdout);                                       \
        }                                                         \
    }while(0)

# define log_warn(fmt, ...)                                       \
    do {                                                          \
        if(LOGV & WARN_ENABLE) {                                  \
            fprintf(stdout, "%-8s "fmt" (%s:%d)\n", "[WARN]",     \
                __VA_ARGS__, __FILE__, __LINE__);                 \
            fflush(stdout);                                       \
        }                                                         \
    }while(0)

# define log_err(fmt, ...)                                        \
    do {                                                          \
        if(LOGV & ERROR_ENABLE) {                                 \
            int err = errno;                                      \
            fprintf(stderr, "%-8s "fmt" (%s:%d)\n", "[ERROR]",    \
                __VA_ARGS__, __FILE__, __LINE__);                 \
            if(err)                                               \
                fprintf(stderr, "\t%s\n", strerror(errno));       \
        }                                                         \
    }while(0)

# define log_fatal(ret, fmt, ...)                                 \
    do {                                                          \
        if(LOGV & ERROR_ENABLE) {                                 \
            int err = errno;                                      \
            fprintf(stderr, "%-8s "fmt" (%s:%d)\n", "[FATAL]",    \
                __VA_ARGS__, __FILE__, __LINE__);                 \
            if(err)                                               \
                fprintf(stderr, "\t%s\n", strerror(errno));       \
            exit(ret);                                            \
        }                                                         \
    }while(0)

#endif
