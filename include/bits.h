#ifndef BITS_H
# define BITS_H

# define BITS_EXTRACT(value, n, offset) \
    ((value >> offset) & ((1 << n) - 1))

#endif
