#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "list.h"

LIST(int, myList);
LIST(int*, myList2);

void freePtr(int* p) { free(p); }

int main(int argc, const char* argv[])
{
    myList_t test;
    node_myList_t node;
    int i;
    LIST_INIT(test, NULL);
    assert(test->size == 0);
    LIST_INSERT_HEAD(test, 1337);
    assert(test->head->data == test->tail->data);
    LIST_REMOVE_HEAD(test);
    assert(test->size == 0 && test->head == NULL && test->tail == NULL);
    for(i = 0; i < 10; ++i) {
        LIST_INSERT_TAIL(test, i);
        LIST_INSERT_HEAD(test, i);
    }
    assert(test->size == 20);
    assert(test->head->data == test->tail->data);
    LIST_INSERT_AT(test, 42, 0);
    LIST_AT(test, 0, node);
    assert(node->data == 42);
    LIST_INSERT_AT(test, 1337, 6);
    LIST_AT(test, 6, node);
    assert(node->data == 1337);
    LIST_REMOVE_AT(test, 6);
    LIST_AT(test, 6, node);
    assert(node->data != 1337);
    i = 0;
    LIST_FOREACH(test)
        ++i;
    assert(i == test->size);
    LIST_FREE(test);

    myList2_t l;
    LIST_INIT(l, freePtr);
    for(i = 0; i < 100; ++i)
        LIST_INSERT_TAIL(l, malloc(sizeof(int)));
    LIST_FREE(l);

    return EXIT_SUCCESS;
}
