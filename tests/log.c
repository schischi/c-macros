#include <stdio.h>
#include <stdlib.h>

#include "log.h"

#define LOGV (LOG_ENABLE | WARN_ENABLE | ERROR_ENABLE)

int main(int argc, const char* argv[])
{
    log_info("%s", "log");
    log_warn("%s", "warn");
    log_err("%s", "error");
    //log_fatal(42, "%s", "fatal");

    return EXIT_SUCCESS;
}
