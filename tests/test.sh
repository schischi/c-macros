#!/bin/sh

check_failure() {
    if [ $? -eq 0 ]; then
        echo "  [DONE]"
    else
        echo "  [FAIL]"
        exit 1
    fi
}

echo -n Compiling
make > /dev/null
check_failure
echo -n Testing list.h
valgrind --quiet --error-exitcode=1 --leak-check=full ./list
check_failure
echo -n Testing vector.h
valgrind --quiet --error-exitcode=1 --leak-check=full ./vector
check_failure
echo -n Testing table.h
valgrind --quiet --error-exitcode=1 --leak-check=full ./table
check_failure
echo "SUCCESS :)"
