#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "hashtbl.h"

int hash(int key)
{
    return key % 10;
}

int cmp(int k1, int k2)
{
    return k1 - k2;
}

HASHTBL(int, int, myTable);

int main(int argc, const char* argv[])
{
    myTable_t test;
    int ret;
    HASHTBL_INIT(test, 10, hash, cmp, NULL);
    HASHTBL_SET(test, 42, 4);
    HASHTBL_SET(test, 41, 14);
    HASHTBL_SET(test, 40, 9);
    HASHTBL_SET(test, 40, 9);
    HASHTBL_SET(test, 39, 19);
    HASHTBL_GET(test, 4, ret);
    assert(ret == 42);
    assert(test->bucket[hash(9)]->size == 3);
    HASHTBL_DEL(test, 9);
    assert(test->bucket[hash(9)]->size == 2);
    HASHTBL_FREE(test);
    return EXIT_SUCCESS;
}
