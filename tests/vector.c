#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "vector.h"

VECTOR(int, myVector);

int main(int argc, const char* argv[])
{
    int i, n;
    myVector_t test;
    VECTOR_CREATE(test, NULL);
    for(i = 0; i < 10; ++i)
        VECTOR_ADD(test, i);
    assert(test->size == 10);
    n = test->array[9];
    VECTOR_DEL(test, 0);
    assert(test->size == 9);
    assert(test->array[0] == n);
    n = test->array[4];
    VECTOR_DEL_ORDER(test, 3);
    assert(test->array[3] == n);
    VECTOR_FOREACH(test, i)
        (void)i;
    assert(i == 10 - 2);
    VECTOR_FREE(test);
    return EXIT_SUCCESS;
}
